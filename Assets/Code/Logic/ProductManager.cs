﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawMeshInstancedCallData
{
    public Matrix4x4[] productMatrices;
    public int numberOfMeshesToRender;
    public Vector4[] colors;
    public MaterialPropertyBlock materialProperties;
};

public class ProductManager : MonoBehaviour {

    public static readonly int instancesPerCall = 1023;
    public static readonly int instancesPerProduct = 50;

    public static List<Product> productInstances = new List<Product>();

    public GameObject productPrefab;
    public int numberOfProducts;
    public Mesh productMesh;
    public MeshRenderer productMeshRenderer;
    public Material productMaterial;

    //Matrix4x4[] productMatrices = new Matrix4x4[1000];
    //public int numberOfProductsToRender;
    public int numberOfMeshesToRender;
    public int numberOfRenderCalls;
    List<DrawMeshInstancedCallData> drawCalls = new List<DrawMeshInstancedCallData>();

    public float elapsedTimeSinceFixedUpdate = 0.0f;
    public float ProductAnimationTime;

    bool renderCallsDirty = true;

    // Use this for initialization
    void Start () {
        if (SystemInfo.supportsInstancing)
        {
            Debug.Log("GPU supports instancing");
        }
        else
        {
            Debug.Log("GPU DOES NOT support instancing");
        }
        var productMeshFilter = productPrefab.GetComponentInChildren<MeshFilter>();
        productMesh = productMeshFilter.sharedMesh;
        productMeshRenderer = productPrefab.GetComponentInChildren<MeshRenderer>();
        //productMaterial = productMeshRenderer.sharedMaterial;

    }

    private void FixedUpdate()
    {
        elapsedTimeSinceFixedUpdate = 0.0f;
        renderCallsDirty = true;
    }

    // Update is called once per frame
    void Update () {
        if (renderCallsDirty)
        {
            numberOfProducts = productInstances.Count;
            int callCounter = 0;
            int meshCounter = 0;
            for (int i = 0; i < numberOfProducts; ++i)
            {
                Product p = productInstances[i];
                Matrix4x4 m = p.transform.localToWorldMatrix;
                Vector3 v = p.currentVelocityDirection;
                for (int j = 0; j < instancesPerProduct; ++j)
                {
                    m[1, 3] += 0.15f;
                    //productMatrices[meshCounter++] = m;
                    if (callCounter >= drawCalls.Count)
                    {
                        DrawMeshInstancedCallData data = new DrawMeshInstancedCallData();
                        data.productMatrices = new Matrix4x4[instancesPerCall];
                        data.numberOfMeshesToRender = 0;
                        data.materialProperties = new MaterialPropertyBlock();
                        productMeshRenderer.GetPropertyBlock(data.materialProperties);
                        data.colors = new Vector4[instancesPerCall];
                        drawCalls.Add(data);
                    }
                    drawCalls[callCounter].productMatrices[meshCounter] = m;
                    //drawCalls[callCounter].colors[meshCounter] = new Vector4((float)i / numberOfProducts, (float)j / instancesPerProduct, 1.0f, 1.0f);
                    drawCalls[callCounter].colors[meshCounter] = new Vector4(v.x, v.y, v.z, (float)j / instancesPerProduct);
                    meshCounter++;
                    if (meshCounter == instancesPerCall)
                    {
                        drawCalls[callCounter].numberOfMeshesToRender = meshCounter;
                        // fill up the next call
                        callCounter++;
                        meshCounter = 0;
                    }
                }
            }
            if (meshCounter > 0)
            {
                drawCalls[callCounter].numberOfMeshesToRender = meshCounter;
                // fill up the next call
                callCounter++;
                meshCounter = 0;
            }
            numberOfRenderCalls = callCounter;
            for (int i = 0; i < numberOfRenderCalls; ++i)
            {
                //drawCalls[i].materialProperties.SetVector("Color", new Vector4(1.0f, 1.0f, 1.0f, 1.0f)); // drawCalls[i].colors);
                drawCalls[i].materialProperties.SetVectorArray("_Color", drawCalls[i].colors);
            }
                renderCallsDirty = false;
        }
        numberOfMeshesToRender = 0;
        for (int i = 0; i < numberOfRenderCalls; ++i)
        {
            UnityEngine.Graphics.DrawMeshInstanced(productMesh, 0, productMaterial,
                drawCalls[i].productMatrices, drawCalls[i].numberOfMeshesToRender,
                drawCalls[i].materialProperties);
            numberOfMeshesToRender += drawCalls[i].numberOfMeshesToRender;
        }

        ProductAnimationTime = System.Math.Min(1.0f,System.Math.Max(0.0f,elapsedTimeSinceFixedUpdate / Time.fixedDeltaTime));
        Shader.SetGlobalFloat("ProductAnimationTime", ProductAnimationTime);
        elapsedTimeSinceFixedUpdate += Time.deltaTime;
    }
}
